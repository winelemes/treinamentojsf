package br.com.devmedia.java.primeiraaplicacaojsf.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.DragDropEvent;
import br.com.appjsfcomhdois.model.Tarefa;
import br.com.appjsfcomhdois.persistence.TarefaDao;

@ManagedBean(name = "tarefaManagedBean")
@ViewScoped
public class TarefaManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Tarefa> tarefas;

	private List<Tarefa> tarefasDone;

	private Tarefa selectedTarefa;

	@PostConstruct
	public void init() {
//		try {
//			new TarefaDao().insert(new Tarefa("primeira tarefa do dia !"));
//			new TarefaDao().insert(new Tarefa("segunda tarefa do dia !"));
//		} catch (Exception e) {
//			setMsgExceptionInInsert();
//		}
		
		tarefas = new TarefaDao().list();
		tarefasDone = new ArrayList<Tarefa>();
		selectedTarefa = new Tarefa();
		
	}

	public void todoToDone(DragDropEvent ddEvent) {
		Tarefa tarefa = ((Tarefa) ddEvent.getData());

		tarefasDone.add(tarefa);
		tarefas.remove(tarefa);
	}

	public void doneToTodo(DragDropEvent ddEvent) {
		Tarefa tarefa = ((Tarefa) ddEvent.getData());

		tarefasDone.remove(tarefa);
		tarefas.add(tarefa);
	}

	public void adicionar() {
		try {
			new TarefaDao().insert(selectedTarefa);
			setMsgTarefaAdicionadaComSucesso();
		} catch (Exception e) {
			setMsgExceptionInInsert();
		}

	}

	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	public List<Tarefa> getTarefasDone() {
		return tarefasDone;
	}

	public Tarefa getSelectedTarefa() {
		return selectedTarefa;
	}

	public void setSelectedTarefa(Tarefa selectedTarefa) {
		this.selectedTarefa = selectedTarefa;
	}

	private void setMsgExceptionInInsert() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Erro interno ao adicionar a tarefa!"));
		context.getExternalContext().getFlash().setKeepMessages(true);
	}
	private void setMsgTarefaAdicionadaComSucesso(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Lembrete adicionado com sucesso!"));
		context.getExternalContext().getFlash().setKeepMessages(true);
	}
}