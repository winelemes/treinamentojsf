package br.com.appjsfcomhdois.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactorySing {
	private static EntityManagerFactory entityManagerFactory;

	public static EntityManagerFactory getEMF() {
		if (entityManagerFactory == null)
			entityManagerFactory = Persistence.createEntityManagerFactory("trello");

		return entityManagerFactory;
	}

}
