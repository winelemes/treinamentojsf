package br.com.appjsfcomhdois.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tarefa {
	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String descricao;
	@Column
	private Date dataAtualizacao;

	public Tarefa() {

	}

	public Tarefa(long id, String descricao, Date dataAtualizacao) {
		this.id = id;
		this.descricao = descricao;
		this.dataAtualizacao = dataAtualizacao;
	}

	public long getId() {
		return id;
	}

	/**
	 * @param descricao
	 */
	public Tarefa(String descricao) {

		this.descricao = descricao;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

}
