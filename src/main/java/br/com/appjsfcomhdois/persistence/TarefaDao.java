package br.com.appjsfcomhdois.persistence;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.appjsfcomhdois.model.Tarefa;
import br.com.appjsfcomhdois.utils.EntityManagerFactorySing;

public class TarefaDao {

	public void insert(Tarefa tarefa) throws Exception {

		EntityManager em = EntityManagerFactorySing.getEMF().createEntityManager();
		try {
			// setando a Data e Hora atual
			tarefa.setDataAtualizacao(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
			em.getTransaction().begin();
			em.persist(tarefa);
			em.getTransaction().commit();
		} catch (Exception e) {
			// TODO: Log exception
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public List<Tarefa> list() {
		EntityManager em = EntityManagerFactorySing.getEMF().createEntityManager();
		try {
			javax.persistence.TypedQuery<Tarefa> q = em.createQuery("from Tarefa", Tarefa.class);
			return q.getResultList();
		} catch (Exception e) {
			// TODO LogError
			return new ArrayList<Tarefa>();
		} finally {
			em.close();
		}
	}
}
